# Nox
Fast calendar app written in ClojureScript.

## Setup
To run the app type in the console: 

```
#!bash

lein figwheel
```
Then point your browser to localhost:3449. That's all!