(ns com.kaicode.nox.color
  (:require [goog.color]))

(defn darken [hex factor]
  (let [color (-> hex
                  (goog.color/hexToRgb)
                  (goog.color/darken (-> factor (min 1.0) (max -1.0))))]
    (goog.color/rgbToHex (aget color 0) (aget color 1) (aget color 2))))

