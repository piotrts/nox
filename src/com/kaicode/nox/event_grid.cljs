(ns com.kaicode.nox.event-grid
  (:require [com.kaicode.nox.db :as db]
            [com.kaicode.nox.interval-trees :refer [interval-tree add-interval query-interval query-group
                                                    interval-tree->seq median median2 median2*]]
            [com.kaicode.nox.color]
            [cljs.core.async :refer [put! promise-chan chan <! >! timeout close!]]
            [cljs.core.async :refer [put! take! promise-chan chan <! >! timeout close!]]
            [reagent.core :as r]
            [cljsjs.moment]))

(defn first-difference [a b]
  (if (= (first a) (first b))
    (first-difference (rest a) (rest b))
    (first (or b a))))

(defn intersects? [[start end] [start' end']]
  (< (max start start') (min end end')))

(def disjoint? (complement intersects?))

(defn index-of-first-disjoint-event [it event]
  (let [grouped (disj (into #{} (map second (interval-tree->seq it))) event)
        taken-indices (keep (fn [event']
                              (when (intersects? [(:event/start event) (:event/end event)]
                                                 [(:event/start event') (:event/end event')])
                                 (:grid-cell/column event')))
                            grouped)]
    (first-difference (sort taken-indices) (range))))

(defn- clamp-event-start [event min-value]
  (if (< (:event/start event) min-value)
    (-> event
        (assoc :event/start min-value)
        (update :nox/clamp (fnil conj #{}) :start))
    event))

(defn- clamp-event-end [event max-value]
  (if (> (:event/end event) max-value)
    (-> event
        (assoc :event/end max-value)
        (update :nox/clamp (fnil conj #{}) :end))
    event))

(defn clamp-event [event min-value max-value]
  (-> event
      (clamp-event-start min-value)
      (clamp-event-end max-value)))

(defn clamp-events [events min-value max-value]
  (map (fn [event]
         (clamp-event event min-value max-value))
       events))

(defn unit->str [unit]
  (name unit))

(defn unit-duration
  "Returns the duration of unit in milliseconds"
  [unit]
  (- (.valueOf (.add (js/moment 0) 1 (unit->str unit)))
     (.valueOf (.add (js/moment 0)))))

(defn start-of [time unit]
  (.. (js/moment time) (startOf (unit->str unit)) valueOf))

(defn end-of [time unit]
  (.. (js/moment time) (endOf (unit->str unit)) valueOf))

(defn instant->number [instant]
  (.valueOf (js/moment instant)))

(defn round-milliseconds [time]
  (-> time
      (/ 1000)
      (js/Math.round)
      (* 1000)))

(defn events->interval-tree [events]
  (reduce (fn [acc event]
            (add-interval acc (:event/start event) (:event/end event) event))
          (interval-tree
            (/ (+ (:event/start (first events)) (:event/end (last events))) ;; are they sorted?)
               2))
          events))

(defn group-events-into-disjoint-blocks [events]
  (if (seq events)
    (let [it (events->interval-tree events)]
      (loop [groups nil
             left (into #{} events)]
        (if (seq left)
          (let [event      (first left)
                new-group  (query-group it (:event/start event) (:event/end event))
                new-left   (into #{} (apply disj left (map second new-group)))]
            (recur (conj groups (sort-by
                                  (juxt :event/start :event/end)
                                  (map second new-group)))
                   new-left))
          groups)))
    '(())))

;; timespan: [start-time end-time]
(defn inclusive-range [start end step]
  (range start (+ end step) step))

(defn time-range
  "Returns a sequence of UNIX timestamps between start-time and end-time,
  by step. Inclusive."
  [start-time end-time unit]
  (inclusive-range start-time end-time (unit-duration unit)))

(defn timespans
  "Returns a sequence of timespan ranges in the form of [start end]
  between start-time and end-time, by step. Inclusive."
  [start-time end-time unit]
  (map vec (partition 2 1 (time-range start-time end-time unit))))

(defn row-map [f grid]
  (into {}
    (map (fn [[k v]]
           [k (f [k v])])
         grid)))

(defn cell-map [f grid]
  (into {}
    (map (fn [[k v]]
           [k (into {}
                (map (fn [[k' events]]
                       (f [k k' events]))
                     v))])
         grid)))

(defn event-map [f grid]
  (into {}
    (map (fn [[k v]]
           [k (into {}
                (map (fn [[k' v']]
                       [k' (map (fn [event]
                                  (f [k k' event]))
                                v')])
                     v))])
         grid)))

(defn block-event-map [f grid]
  (into {}
    (map (fn [[k v]]
           [k (into {}
                (map (fn [[k' v']]
                       [k' (map (fn [block]
                                  (map (fn [event]
                                         (f [k k' event]))
                                       block))
                                v')])
                     v))])
         grid)))

(defn block-event-map-indexed [f grid]
  (into {}
    (map-indexed (fn [i [k v]]
                   [k (into {}
                        (map-indexed (fn [i' [k' v']]
                                       [k' (map (fn [block]
                                                  (map (fn [event]
                                                         (f [i i'] [k k' event]))
                                                       block))
                                                v')])
                                     v))])
                 grid)))

(defn normalize-event [event]
  (-> event
      (update :event/start (comp round-milliseconds instant->number))
      (update :event/end (comp round-milliseconds instant->number))))

(defn normalize-events [events]
  (map normalize-event events))

(defn populate-grid [events grid]
  (let [it (events->interval-tree events)]
    (cell-map (fn [[_ k' _]]
                [k' (vals (query-interval it k'))])
              grid)))

(defn clamp-grid [grid]
  (event-map (fn [[k k' event]]
               (clamp-event event (first k') (second k')))
             grid)) 

(defn group-into-blocks [grid]
  (cell-map (fn [[_ k' events]]
              [k' (group-events-into-disjoint-blocks events)])
            grid))

(defn assoc-block-columns [grid block-position-fn]
  ;; TODO use block-map
  (event-map (fn [[k k' block]]
               (block-position-fn k k' block))
             grid))

(defn assoc-max-block-columns [grid]
  (event-map (fn [[_ _ block]]
               (let [max-block-column (apply max (map :grid-cell/column block))]
                 (map (fn [event]
                        (assoc event :grid-cell/column-count max-block-column))
                      block)))
             grid))
               
(defn assoc-real-positions [grid position-fn limited-width? drawable-area-width drawable-area-height]
  (block-event-map (fn [[_ k' event]]
                     (position-fn event k' limited-width? drawable-area-width drawable-area-height))
                   grid))

(defn assoc-grid-cells [grid grid-cell-fn]
  (grid-cell-fn grid))

(defn flatten-blocks [grid]
  (->> grid vals (map vals) flatten))

;; TODO this should be a transducer
(defn default-block-columns-fn [k k' block]
  (-> (reduce (fn [it event]
                (let [event' (assoc event
                               :grid-cell/column (index-of-first-disjoint-event it event))]
                  (add-interval it (:event/start event') (:event/end event') event')))
              (interval-tree (median2* (:event/start (first block)) (:event/end (last block))))
              block)
      interval-tree->seq
      vals))

(defn assoc-z-indices
  "Assocs :nox/z-index keys. The longer the event lasts, or its position
  is closer to the top-left, the lower the z-index."
  [events]
  (->> events
       (sort-by (fn [event]
                  (- (:event/start event) (:event/end event))))
       (map-indexed (fn [i event]
                      (assoc event :nox/z-index i)))))

(defn spy
  ([x]
   (prn x)
   x)
  ([x debug-str]
   (js/console.log debug-str)
   (prn x)
   x))

(defn grid [state-ref empty-grid params]
  (let [{:keys [time unit limited-width? drawable-area-width drawable-area-height empty-grid-fn
                time-range-fn grid-cell-fn block-position-fn position-fn
                post-process-fn]} params
        [start-time end-time] (time-range-fn time)]
     (-> (db/query-events-by-timespan state-ref start-time end-time)
         (normalize-events)
         (populate-grid empty-grid)
         (clamp-grid)
         (group-into-blocks)
         (assoc-grid-cells grid-cell-fn)
         (assoc-block-columns block-position-fn)
         (assoc-max-block-columns)
         (assoc-real-positions position-fn limited-width? drawable-area-width drawable-area-height)
         (flatten-blocks)
         (assoc-z-indices)
         (post-process-fn))))
         

(defn empty-weekly-grid-data [time columns-unit column-unit row-unit]
  (let [start-time (start-of time columns-unit)
        end-time (end-of time columns-unit)]
    (map (fn [[start-time' end-time' :as columnspan]]
           [columnspan (map (fn [rowspan]
                              [rowspan nil])
                            (timespans start-time' end-time' row-unit))])
         (timespans start-time end-time column-unit))))



