(ns com.kaicode.nox.interval-trees)

(defn -interval-tree [m l r v]
  #js [m l r v])

(defn- median [it] (aget it 0))
(defn- left [it] (aget it 1))
(defn- right [it] (aget it 2))
(defn- values [it] (aget it 3))

(defn- median2* [a b]
  (/ (+ a b) 2))

(defn- median2 [m]
  (/ (+ (aget m 0) (aget m 1)) 2)) 

(defn- median3 [m]
  (/ (reduce (fn [acc e]
               (+ acc (median2 e)))
             0 m)
     (.-length m))) 

(defn- it-median [it]
  (median3 (values it)))

(defn -add-interval [it a b v]
  (if it
    (let [m (median it)]
      (cond
        (and (>= a m) (>= b m))
        (do (aset it 2 (-add-interval (right it) a b v))
            it)
        (and (<= a m) (<= b m))
        (do (aset it 1 (-add-interval (left it) a b v))
            it)
        ;;(and (<= a m) (>= b m))
        :otherwise
        (do
          (.push (values it) #js [a b v])
          it)))
    (-interval-tree (median2* a b) nil nil #js [#js [a b v]])))

(defn interval-tree
  ([m]
   (-interval-tree m nil nil #js []))
  ([m & data]
   (let [it (interval-tree m)]
     ;; this should be optimized, so we're not traversing a tree
     ;; each time we add something to it
     (doseq [[a b v] (partition 3 data)]
       (-add-interval it a b v))
     it)))

(defn add-interval [it a b v]
  (-add-interval it a b v))

(defn query-point [it p]
  (concat
   (filter (fn [e]
             (and (<= (aget e 0) p)
                  (>= (aget e 1) p)))
           (array-seq (values it)))
   (if (< p (median it))
     (when (left it)
       (query-point (left it) p))
     (when (right it)
       (query-point (right it) p)))))

;; used my own ideas how it should work, are there any better solutions?
(defn query-interval* [it a b]
;  (js/console.log "LOOKING FOR" a b)
  (when it
    (concat
     (filter (fn [e]
               (or (and (<= (aget e 0) a) (< a (aget e 1)))
                   (and (< (aget e 0) b) (<= b (aget e 1)))
                   (<= a (median2 e) b)))
             (array-seq (values it)))
     (when (<= a (median it)) 
       (query-interval* (left it) a b))
     (when (<= (median it) b) 
       (query-interval* (right it) a b)))))

(defn normalize [query-result]
  (map (fn [e]
         (if (and (aget e 0) (aget e 1))
           [[(aget e 0) (aget e 1)] (aget e 2)]))
       query-result))

(defn query-interval
  ([it v]
   (normalize (query-interval* it (first v) (second v))))
  ([it a b]
   (normalize (query-interval* it a b))))

(defn overlaping-interval [[interval & intervals]]
  (reduce (fn [[minv maxv] interval]
            (let [[a b] interval]
              [(if (< a minv) a minv)
               (if (> b maxv) b maxv)]))
          interval
          intervals))

(defn query-group [it a b & [max-iter]]
  (loop [iter 0 acc (into #{} (normalize (query-interval* it a b)))]
    (let [overlaping (overlaping-interval (map first acc))
          new-acc (into #{} (normalize (query-interval* it (first overlaping) (second overlaping))))]
      (if (or (>= iter (or max-iter 100)) (= acc new-acc))
        new-acc
        (recur (inc iter) new-acc)))))

(defn interval-tree->seq [it]
  (when it
    (concat
     (normalize (values it))
     (interval-tree->seq (left it))
     (interval-tree->seq (right it)))))

(defn- js-equal? [a b]
  (js* "(!(~{a} < ~{b} || ~{a} > ~{b}))"))

;(deftest interval-tree-tests
;  (testing "invertal-tree creation tests"
;    (is (js-equal? (interval-tree 0.5 0 1 :small-interval)
;                   #js [0.5 nil nil #js [#js [0 1 :small-interval]]]))
;    (is (js-equal? (add-interval
;                     (interval-tree 10 5 15 :bigger-interval)
;                     1 2 :small-interval)
;                   #js [10 #js [1.5 nil nil #js [#js [1 2 :small-interval]]]
;                        nil
;                        #js [#js [5 15 :bigger-interval]]])))
;  (testing "interval-tree to sequence conversion"
;    (is (= (interval-tree->seq
;             (interval-tree 20 10 30 :bigger-interval))
;           [[[10 30] :bigger-interval]])))
;  (testing "interval-tree queries"
;    (let [it (-> (interval-tree 50)
;                 (add-interval 10 20 :smaller-interval))]
;      (testing "intersection"
;        (is (= (query-interval it 10 20)
;               '([[10 20] :smaller-interval])))
;        (is (= (query-interval it 15 25)
;               '([[10 20] :smaller-interval])))))))
                

;(interval-tree-tests)

(comment
  (def interval-hehe-tree
    (-> (interval-tree 50)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 300 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval -30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 90 150 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das)
        (add-interval 20 30 :right)
        (add-interval 10 15 :e)
        (add-interval 40 60 :left-side)
        (add-interval 30 50 :das))))

;(query-interval interval-hehe-tree 0 500)

;(def events
;  [{:db/id 1
;    :event/type 1 ;:event.type/default
;    :event/start 1410715640579 
;    :event/end   1410715940579 
;    :event/desc "An event that takes about a week"}
;   {:db/id 2
;    :event/type 1 ;:event.type/default
;    :event/start 1410715740579 
;    :event/end 1410719640579 
;    :event/desc "An event that takes a day"}
;   {:db/id 3
;    :event/type 1 ;:event.type/default
;    :event/start 1410718540579 
;    :event/end   1410720640579 
;    :event/desc "An event that takes 30 days"}
;   {:db/id 4
;    :event/type 1 ;:event.type/default
;    :event/start 1410715640579 
;    :event/end 1410715640579 
;    :event/desc "An event that takes 8 hours"}])

;(defn interval-tree []
;  (sorted-map))

;(defn add-interval [it start end v]
;  (merge-with concat it {[start end] (seq [v])}))

;(defn intersects? [[start end] [start' end']]
;  (< (max start start') (min end end')))

;(defn query-interval [it start end]
;  (into (interval-tree)
;        (filter (comp
;                 (partial intersects? [start end])
;                 key)
;                it)))


;(defn query-interval-recursive
;  "Returns intersecting ranges along with the ranges that intersect with these ranges,
;  recursing until no more intersecting ranges are found."
;  ([it start end]
;   (query-interval-recursive it start end (constantly true)))
;  ([it start end constraint-fn]
;   (seq
;    (loop [iter 0
;           acc (sorted-map)
;           left (filter constraint-fn (query-interval it start end))]
;      (let [found (into (sorted-map)
;                                (map (fn [interv]
;                                       (let [[start end] (key interv)]
;                                         (filter constraint-fn (query-interval it start end))))
;                                     left))]
;        (if (< iter 5000) ;; to make sure it completes
;          (if (= (keys found)
;                 (some (into #{} (keys found)) ;; like (every?) but also makes sure
;                       (into #{} (keys acc)))) ;; that the second param to (some) is not nil
;            (apply conj acc found)
;            (recur (inc iter)
;                   (into (sorted-map) (apply conj acc found))
;                   found))
;          (apply conj acc found)))))))
;
;(defn t []
;(-> (interval-tree)
;    (add-interval  1  2 :hehe)
;    (add-interval 10 20 :a)
;    (add-interval 10 20 :c)
;    (add-interval 15 30 :b)
;    (add-interval 18 50 :x)
;    (add-interval 49 60 :y)
;    (add-interval 70 80 :z)
;    (query-interval-recursive 59 71))
;)
;(t)
;
;
;(defn event->vec [event]
;  [[(:event/start event) (:event/end event)] event])
;
;(defn events->vec [events]
;  (mapcat event->vec events))
