(ns com.kaicode.nox.db
   (:require [datascript.core :as d]
             [datascript.db :as db]))

;; TODO find a better name
(defn intersects+after? [start-time start-time' end-time']
  (or (< start-time start-time')
      (< start-time end-time'))) 

(defn intersects? [start-time end-time start-time' end-time']
  (< (max start-time start-time') (min end-time end-time')))

;; TODO remove this fn
(defn query-event-by-id [state-ref eid]
  (first
    (filter (fn [event]
              (= eid (:system/id event))) ;; ?
            (:events @state-ref))))

(defn query-event-by-entity-kw [state-ref entity-kw eid]
  (first
    (filter (fn [event]
              (= eid (entity-kw event)))
            (:events @state-ref))))

(defn query-events-all [state-ref]
  (:events @state-ref))

(defn query-events-by-timespan 
  ([state-ref start-time]
   (let [filter-fn (fn [start-time' end-time']
                     (intersects+after? start-time start-time' end-time'))]
     (filter (fn [event]
               (filter-fn (:event/start event)
                          (:event/end event)))
             (:events @state-ref))))
  ([state-ref start-time end-time]
   (let [filter-fn (fn [start-time' end-time']
                     (intersects? start-time' end-time' start-time end-time))]
     (filter (fn [event]
               (filter-fn (:event/start event)
                          (:event/end event)))
             (:events @state-ref)))))


