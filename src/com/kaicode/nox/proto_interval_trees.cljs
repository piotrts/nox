(ns com.kaicode.nox.proto-interval-trees)

;; Extremely naive interval trees implementation whose only purpose is to provide
;; functionalities associated with interval trees and appropriate function names.
;; It is implemented using simple sorted-maps.
;; This should be replaced by a third-party interval trees implementation,
;; when a good one (or at least a working one) is released. Unfortunately
;; I couldn't find any.

(defn interval-tree []
  (sorted-map))

(defn add-interval [it start end v]
  (merge-with concat it {[start end] (seq [v])}))

(defn intersects? [[start end] [start' end']]
  (< (max start start') (min end end')))

(def disjoint? (complement intersects?))

(defn query-interval [it start end]
  (into (interval-tree)
        (filter (comp
                 (partial intersects? [start end])
                 key)
                it)))

;; returns the first intersecting interval
(defn query-interval-first [it start end]
   (first
        (filter (comp
                 (partial intersects? [start end])
                 key)
                it)))

(defn query-interval-recursive
  "Returns intersecting ranges along with the ranges that intersect with these ranges,
  recursing until no more intersecting ranges are found."
  ([it start end]
   (query-interval-recursive it start end (constantly true)))
  ([it start end constraint-fn]
   (seq
    (loop [iter 0
           acc (sorted-map)
           left (query-interval it start end)]
      (let [found (into (sorted-map)
                              (map (fn [interv]
                                     (let [[start end] (key interv)]
                                       (query-interval it start end)))
                                   left))]
        (if (< iter 5000) ;; to make sure it completes
          (if (= (keys found)
                 (some (into #{} (keys found)) ;; like (every?) but also makes sure
                       (into #{} (keys acc)))) ;; that the second param to (some) is not nil
            (apply conj acc found)
            (recur (inc iter)
                   (into (sorted-map) (apply conj acc found))
                   found))
          (apply conj acc found)))))))

(defn interval-tree->vals [it]
  (apply concat (map second it)))

