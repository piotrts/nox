(ns com.kaicode.nox.core
  (:require [com.kaicode.mercury :as m]
            [com.kaicode.nox.db :as db]
            [com.kaicode.nox.event-grid :as event-grid]
            [com.kaicode.nox.interval-trees :refer [interval-tree add-interval query-interval query-group
                                                    interval-tree->seq median median2 median2*]]
            [com.kaicode.nox.color]
            [cljs.core.async :refer [put! take! promise-chan chan <! >! timeout close!]]
            [reagent.core :as r]
            [cljsjs.moment]
            [cljs-uuid-utils.core :as uuid]
            [goog.events :as gevents]
            [goog.dom :as gdom]
            [goog.style :as gstyle]
            [cljsjs.hammer]
            [mobile-detect]
            [bubble.core :as bubble])
  (:import [goog.events EventTarget])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(def first-column-width 50)

(def event-margin 1)

(def event-radius 7)
(def horizontal-event-padding 3)
(def vertical-event-padding 0)

(def hour-format-string "hha")
(def hour-minutes-format-string "hh:mma")

(def limited-width-upper-bound 400)

;; ----------------------------------------------------------------------------
(def ^:private double-event-margin (* event-margin 2))
(def ^:private triple-event-margin (* event-margin 3))

(def entity-kw :system/id)

(defn gen-entity-kw []
  (uuid/uuid-string (uuid/make-random-uuid)))

(defn- format-moments
  "Returns specified js/moment value (or a range of values) formated
  using one of the default formatters"
  ([a formatter]
   (.format (js/moment a) formatter))
  ([a b formatter]
   (str (.format (js/moment a) formatter) " - " (.format (js/moment b) formatter))))

(defn- get-current-time
  "Helper function returning current time stored in state"
  [state-ref]
  (:current-time @state-ref))

(defn set-current-time!
  "Sets current time in state ref to specified value"
  [state-ref time]
  (swap! state-ref assoc :current-time time))

(defn- unit->str
  "Helper function converting unit to string"
  [unit]
  (name unit))

(defn start-of [time unit]
  (.. (js/moment time) 
      ;; this line below fixes a moment.js bug where calls like
      ;; (-> some-time (start-of :month) (start-of :week)) were breaking utcOffsets
      (utcOffset (.utcOffset (js/moment)))
      (startOf (unit->str unit)) valueOf))

(defn end-of [time unit]
  (.. (js/moment time) (utcOffset (.utcOffset (js/moment))) (endOf (unit->str unit)) valueOf))

(defonce
  ^{:private true
    :doc "List of all possible units used by moment.js"}
  all-units
  '(:second :minute :hour :day :week :month :year)) 

(defonce
  ^{:private true
    :doc "Hash-map containing moment.js units as keys
         and their duration as values. Used as a cache."}
  unit-durations
  (into {}
    (map (fn [unit]
           [unit (- (.valueOf (.add (js/moment 0) 1 (unit->str unit)))
                    (.valueOf (.add (js/moment 0))))])
         all-units)))

(defn- unit-duration
  "Returns the duration of unit in milliseconds"
  [unit]
  (get unit-durations unit))

;; TODO bad name, I didn't know what this function does after a while
(defn get-utc-time-unix
  "Returns hour, minutes, and minutes of specified UNIX point
  in time also as UNIX instant"
  [time]
  (let [m1 (.. js/moment (utc time))]
    (* (+ (* 60 60 (.hour m1))
          (* 60 (.minutes m1))
          (.seconds m1))
       1000)))

(defn get-time-unix
  "Returns hour, minutes, and minutes of specified UNIX point
  in time also as UNIX instant"
  [time]
  (let [m1 (js/moment time)]
    (* (+ (* 60 60 (.hour m1))
          (* 60 (.minutes m1))
          (.seconds m1))
       1000)))

(def get-local-time-unix get-time-unix)

(defn inclusive-range [start end step]
  (range start (+ end step) step))

(defn time-range
  "Returns a sequence of UNIX timestamps between start-time and end-time,
  by step. Inclusive."
  [start-time end-time unit]
  (inclusive-range start-time end-time (unit-duration unit)))

(defn timespans
  "Returns a sequence of timespan ranges in the form of [start end]
  between start-time and end-time, by step. Inclusive."
  [start-time end-time unit]
  (map vec (partition 2 1 (time-range start-time end-time unit))))

(defn- fix-event [event]
  (let [event (if (:event/type event)
                event
                (assoc event :event/type 1))
        event (if (entity-kw event)
                event
                (assoc event entity-kw (gen-entity-kw)))
        event (if (inst? (:event/start event))
                event
                (assoc event :event/start (js/Date. (:event/start event))))
        event (if (inst? (:event/end event))
                event
                (assoc event :event/end (js/Date. (:event/end event))))
      ]
    event))


(defn correct-event? [event]
  (let [{:keys [event/start event/end event/title event/type]} event]
    (and type
         (not (clojure.string/blank? title))
         (< start end))))

(defn ->local-time [time]
  (+ time (* 1000 60 60 (.utcOffset (js/moment)))))

(defn add-event
  "Adds an event. Event must contain :event/start, :event/end, :event/type and :event/desc keys."
  [state-ref event]
  (when (correct-event? event)
    (let [fixed-event (fix-event event)]
      (swap! state-ref update :events conj fixed-event)
      ((:remote-transact-fn @state-ref) [fixed-event]))))

(defn add-events
  "Adds multiple events. See docs for add-event."
  [state-ref events]
  (let [filtered (filterv (fn [event]
                            (when (correct-event? event)
                              (fix-event event)))
                           events)]
    (swap! state-ref update :events (fn [events']
                                      (apply conj events' filtered)))
    ((:remote-transact-fn @state-ref) filtered)))

(defn edit-or-add-event [state-ref event]
  (when (correct-event? event)
    (if (db/query-event-by-entity-kw state-ref entity-kw (entity-kw event))
      (when (correct-event? event)
        (let [fixed-event (fix-event event)]
          (swap! state-ref assoc :events (keep (fn [event']
                                                 (if (= (entity-kw event) (entity-kw event'))
                                                   fixed-event
                                                   event'))
                                               (:events @state-ref)))
          ((:remote-transact-fn @state-ref) [fixed-event])))
      (add-event state-ref event))))

(defn remove-event
  "Removes event"
  [state-ref eid] ;; TODO check the correct name of the argument
  ;; O(n), not good. After removing datascript we should store all the events as interval tree
  ;; to make querying / removing faster. Updates should be incrementa.
  (swap! state-ref update :events (fn [events]
                                    (remove (fn [event]
                                              (= (:system/id event) eid))
                                            events)))
  ((:remote-transact-fn @state-ref) [[:db.fn/retractEntity [:system/id eid]]]))

(declare daily-view weekly-view monthly-view agenda-view edit-dialog agenda-view-delete-dialog)


;; daily view
;; ----------------------------------------------------------------------------
(defn daily-view-format-fn [state-ref]
  (format-moments (get-current-time state-ref) (:default-moment-format-str @state-ref)))

(defn daily-view-grid-cell-fn [grid]
  (event-grid/block-event-map (fn [[_ _ event]]
                                (assoc event :grid/row 0
                                             :grid/column 0))
                              grid))

(defn width-delta [event limited-width? drawable-area-width]
  (if limited-width?
    (* (Math/pow (:grid-cell/column-count event) 2)
       (Math/sqrt (- limited-width-upper-bound (- drawable-area-width first-column-width))))
    0))

(defn daily-view-position-fn [event _ limited-width? drawable-area-width drawable-area-height]
  {:post [(pos? (:width %))
          (pos? (:height %))]}
  (let [column (:grid-cell/column event)
        column-count (:grid-cell/column-count event)]
    (assoc event
           :width (-> (- drawable-area-width first-column-width)
                      (+ (width-delta event limited-width? drawable-area-width))
                      (/ (inc column-count))
                      (- (if (= column column-count)
                           triple-event-margin
                           event-margin)))
           :height (-> (- (:event/end event) (:event/start event))
                       (get-utc-time-unix)
                       (/ (unit-duration :day))
                       (* drawable-area-height)
                       (- triple-event-margin))
           :x (-> (- drawable-area-width first-column-width)
                  (* (/ column (inc column-count)))
                  (+ (* (/ column (inc column-count))
                        (width-delta event limited-width? drawable-area-width)))
                  (- (if (pos? column)
                       (/ (* column (width-delta event limited-width? drawable-area-width))
                          column-count)
                       0))
                  (+ event-margin))
           :y (-> (:event/start event)
                  (get-local-time-unix)
                  (/ (unit-duration :day))
                  (* drawable-area-height)
                  (+ event-margin)))))

(defn daily-view-position->time-range-fn [state-ref x y w h]
  (let [renderer    (-> @state-ref :refs :renderer)
        time        (get-current-time state-ref)
        day-start-time (start-of time :day)
        day-end-time   (end-of time :day)
        new-time (-> (/ y h) 
                     (* (unit-duration :day))
                     (+ day-start-time))]
    [(start-of new-time :hour) (inc (end-of new-time :hour))]))

(defn daily-view-time-range-fn [time]
  [(start-of time :day) (end-of time :day)])

(defn daily-view-update-limited-width?-fn [state-ref]
  (swap! state-ref assoc :limited-width?
         (< (- (.-clientWidth (-> @state-ref :refs :main-component))
               first-column-width)
            limited-width-upper-bound)))

(def ^:private daily-view-opts 
  {:render-fn #'daily-view
   :format-fn #'daily-view-format-fn
   :time-range-fn #'daily-view-time-range-fn
   :grid-cell-fn #'daily-view-grid-cell-fn
   :block-position-fn #'event-grid/default-block-columns-fn
   :position-fn  #'daily-view-position-fn
   :post-process-fn #'identity
   :position->time-range #'daily-view-position->time-range-fn
   :update-limited-width?-fn #'daily-view-update-limited-width?-fn
   :name "daily-view"
   :string "Daily"})


;;weekly view
;; ----------------------------------------------------------------------------
(defn weekly-view-format-fn [state-ref]
  (format-moments (start-of (get-current-time state-ref) :week)
                  (end-of (get-current-time state-ref) :week)
                  (:default-moment-format-str @state-ref)))

(defn weekly-view-position-fn [event [day-start-time day-end-time] limited-width? drawable-area-width drawable-area-height]
  {:post [(pos? (:width %))
          (pos? (:height %))]}
  (assoc event
         :width  (-> (/ 1 (inc (:grid-cell/column-count event)))
                     (/ 7)
                     (* (- drawable-area-width first-column-width))
                     (- (if (= (:grid-cell/column event) (:grid-cell/column-count event))
                          triple-event-margin
                          event-margin)))
         :height (-> (- (:event/end event) (:event/start event))
                     (/ (unit-duration :day))
                     (* drawable-area-height)
                     (- triple-event-margin))
         :x (-> (:grid/column event)
                (+ (/ (:grid-cell/column event)
                      (inc (:grid-cell/column-count event))))
                (/ 7)
                (* (- drawable-area-width first-column-width))
                (+ event-margin))
         :y (-> (:event/start event)
                (get-time-unix)
                (/ (unit-duration :day))
                (* drawable-area-height)
                (+ event-margin))))

(defn- weekly-view-grid-cell-fn [grid]
  (event-grid/block-event-map (fn [[_ _ event]]
                                (assoc event :grid/row 0
                                             :grid/column (-> (:event/start event)
                                                              (- (start-of (:event/start event) :week))
                                                              (/ (unit-duration :day))
                                                              int)))
                              grid))

(defn weekly-view-position->time-range-fn [state-ref x y w h]
  (let [renderer    (-> @state-ref :refs :renderer)
        time        (get-current-time state-ref)
        x           (- x first-column-width)
        w           (- w first-column-width)
        fst-day-start-time (start-of (start-of time :week) :day)
        fst-day-end-time   (end-of (start-of time :week) :day)
        day-duration (unit-duration :day)
        new-time    (-> (/ y h)
                        (* day-duration)
                        (+ fst-day-start-time))
        week-day    (-> (/ x w)
                        (* 7)
                        int)]
    [(+ (start-of new-time :hour)
        (* week-day day-duration))
     (inc (+ (end-of new-time :hour)
             (* week-day day-duration)))]))

(defn weekly-view-time-range-fn [time]
  [(start-of time :week) (end-of time :week)])

(defn weekly-view-update-limited-width?-fn [state-ref]
  (swap! state-ref assoc :limited-width?
         (< (-> (.-offsetWidth (-> @state-ref :refs :main-component-parent))
                (- first-column-width)
                (/ 7))
            limited-width-upper-bound)))

(def ^:private weekly-view-opts
  {:render-fn #'weekly-view
   :format-fn #'weekly-view-format-fn
   :time-range-fn #'weekly-view-time-range-fn
   :grid-cell-fn #'weekly-view-grid-cell-fn
   :position-fn #'weekly-view-position-fn
   :block-position-fn #'event-grid/default-block-columns-fn
   :post-process-fn #'identity
   :position->time-range #'weekly-view-position->time-range-fn
   :update-limited-width?-fn #'weekly-view-update-limited-width?-fn
   :name "weekly-view"
   :string "Weekly"})


;; monthly view
;; ----------------------------------------------------------------------------
(defn monthly-view-format-fn [state-ref]
  (format-moments (get-current-time state-ref)
                  (:default-monthly-moment-format-str @state-ref)))

(defn monthly-view-grid-cell-fn [grid]
  (event-grid/block-event-map-indexed (fn [[i i'] [k k' event]]
                                        (assoc event
                                               :grid/row i
                                               :grid/column i'))
                                      grid))

(defn monthly-view-position-fn [event _ limited-width? drawable-area-width drawable-area-height]
  (let [event-height (- 22 event-margin)]
    (assoc event
           :width (-> drawable-area-width
                      (* (/ 1 7))
                      (- triple-event-margin))
           :height event-height
           :x (-> drawable-area-width
                  (/ 7)
                  (* (:grid/column event))
                  (+ event-margin))
           :y (-> drawable-area-height
                  (/ 6)
                  (* (:grid/row event))
                  (+ event-margin)))))


(defn monthly-view-post-process-fn [events]
  (reduce (fn [acc [k events]]
            (let [c (count events)]
              (if (<= c 5)
                (into acc (map-indexed (fn [i event]
                                         (update event :y + (* i (:height event))))
                                       events))
                (into acc (map-indexed (fn [i event]
                                         (let [event (update event :y + (* (inc i) (:height event)))]
                                           (if (= i 4)
                                             (-> event
                                                 (assoc :event/type 0
                                                        :event/desc "more...")
                                                 #_(dissoc :db/id))
                                             event)))
                                       (take 5 events))))))
          nil
          (group-by (juxt :grid/column :grid/row) events)))

(defn monthly-view-position->time-range-fn [state-ref x y w h]
  (let [renderer       (-> @state-ref :refs :renderer)
        time           (get-current-time state-ref)
        week-duration  (unit-duration :week)
        first-day-start-time (start-of (start-of time :month) :week)
        seventh-day-end-time (end-of (start-of time :month) :week)
        new-time       (-> (/ x w)
                           (* (unit-duration :week))
                           (+ first-day-start-time))
        week           (-> (/ y h)
                           (* 6)
                           int)]
    [(+ (start-of new-time :day)
        (* week week-duration))
     (+ (end-of new-time :day)
        (* week week-duration))]))

(defn monthly-view-time-range-fn [time]
  [(-> time (start-of :month) (start-of :week))
   (-> time (end-of :month) (end-of :week))])

(defn monthly-view-update-limited-width?-fn [state-ref]
  (swap! state-ref assoc :limited-width?
         (< (/ (.-offsetWidth (-> @state-ref :refs :main-component-parent)) 7)
            limited-width-upper-bound)))

(def ^:private monthly-view-opts
  {:render-fn #'monthly-view
   :format-fn #'monthly-view-format-fn
   :time-range-fn #'monthly-view-time-range-fn
   :grid-cell-fn #'monthly-view-grid-cell-fn
   :position-fn #'monthly-view-position-fn
   :block-position-fn #'event-grid/default-block-columns-fn
   :post-process-fn #'monthly-view-post-process-fn
   :position->time-range #'monthly-view-position->time-range-fn
   :update-limited-width?-fn #'monthly-view-update-limited-width?-fn
   :name "monthly-view"
   :string "Monthly"})

(def ^:private agenda-view-opts
  {:render-fn #'agenda-view
   :format-fn (constantly nil)
   :name "agenda-view"
   :string "Agenda"})

(def
  ^{:private true
    :doc "Component that is rendered when there are no events
         to show in the agenda view"}
  agenda-view-empty?-content
  [:div {:style {:padding "10px 12px"}}
   "No upcoming events."])

(def
  ^{:private true
    :doc "Map containing available views"}
  view-map {:daily-view daily-view-opts
            :weekly-view weekly-view-opts
            :monthly-view monthly-view-opts
            :agenda-view agenda-view-opts})

(def
  ^{:private true
    :doc "Map containing available dialogs"}
  dialog-map
  {:edit-dialog
   {:render-fn #'edit-dialog
    :name "edit-dialog"}
   :agenda-view-delete-dialog
   {:render-fn #'agenda-view-delete-dialog
    :name "agenda-view-delete-dialog"}})

(def
  ^{:private true
    :doc "View that is rendered after not existing view
          has been requested"}
  unknown-view
  [:span "This view does not exist"])


(defn- add
  "Adds n units to specified time"
  [time n unit]
  (+ time (* (unit-duration unit) n)))

(defn- subtract
  "Adds n units to specified time"
  [time n unit]
  (- time (* (unit-duration unit) n)))

(defn- add-to-current-time!
  "Adds n units to current time specified in state"
  [state-ref n unit]
  (swap! state-ref update :current-time + (* (unit-duration unit) n)))

(defn- subtract-from-current-time!
  "Subtracts n units from current time specified in state"
  [state-ref n unit]
  (swap! state-ref update :current-time - (* (unit-duration unit) n)))

(defn- number->instant
  "Converts a number to an instant"
  [n]
  (-> n (js/moment) (.format) (js/Date.)))

(defn- instant->number
  "Converts an instant to a number"
  [instant]
  (.valueOf (js/moment instant)))

(defn- create-mdl-load-channel!
  "Creates and returns a promise that receives a boolean
  after MDL has been successfully loaded (or not)"
  [state-ref]
  (let [ch (promise-chan)]
    (swap! state-ref assoc :mdl-load-chan ch)
    (go-loop []
      (if (exists? (.-upgradeDom js/componentHandler))
        (put! ch true)
        (do (<! (timeout 100))
            (recur))))
    ch))

(defn- add-mdl-load-listener!
  "Calls the specified function after MDL has been successfully loaded"
  [state-ref f]
  (when-let [ch (:mdl-load-chan @state-ref)]
    (take! ch f)))

(defn- upgrade-dom!
  "Calls componentHandler.upgradeDom() when MDL has been loaded"
  [state-ref]
  (add-mdl-load-listener! state-ref (fn [_] (.upgradeDom js/componentHandler))))

(defn- upgrade-element!
  "Calls componentHandler.upgradeElement() when MDL has been loaded"
  [state-ref element]
  (add-mdl-load-listener! state-ref (fn [_] (.upgradeElement js/componentHandler element))))

(defn drawable-area-width
  "Returns the width of the available area"
  [state-ref]
  (:drawable-area-width @state-ref))

(defn drawable-area-height
  "Returns the height of the available area"
  [state-ref]
  (:drawable-area-height @state-ref))

(defn set-view!
  "Sets the current view. Note that view-key must be a key pointing
  to an existing view. If not, a warning will be displayed."
  [state-ref view-key]
  (swap! state-ref assoc :view (get view-map view-key unknown-view)))

(defn set-dialog!
  "Sets the current dialog. Note that diialog-key must be a key pointing
  to an existing dialog. If not, nothing will be shown."
  [state-ref dialog-key]
  (swap! state-ref assoc :dialog (get dialog-map dialog-key)))

(defn- link
  "Returns a link reagent component, that can either call specified
  function when clicked, or redirect to a specified URL"
  [uri-or-fn val]
  (if (fn? uri-or-fn)
    [:a {:href "#" :on-click uri-or-fn} val]
    [:a {:href uri-or-fn} val]))

(defn- mdl-menu-item
  "Helper function returning a MDL-compatible menu-item"
  [uri-or-fn val]
  [link uri-or-fn [:li.mdl-menu__item val]])

(defn- timespan
  "Returns a :span element containing formated time or timespan according
  to a value set in the current view"
  [state-ref]
  [:span ((get-in @state-ref [:view :format-fn]) state-ref)])
   
(defn- icon
  "Helper function returning MDL icon with specified value"
  [val]
  [:i.material-icons val])

(defn- button
  "Helper function returning an MDL button component with specified value"
  [val]
  [:button {:class "mdl-button mdl-js-button mdl-button--icon"}
   val])

(defn- round-button
  "Helper function returning an MDL round button component"
  [val]
  [:button {:class "mdl-button mdl-js-button mdl-button--fab
                   mdl-button--mini-fab mdl-button--colored button-table"}
   val])

(defn- switcher
  "Switcher component. Renders a formatted date along with arrows that change time."
  [state-ref boundary]
  [:h1.mdl-card__title-text
   [link #(subtract-from-current-time! state-ref 1 boundary)
    [button [icon "keyboard_arrow_left"]]]
   [timespan state-ref]
   [link #(add-to-current-time! state-ref 1 boundary)
    [button [icon "keyboard_arrow_right"]]]])

(defn- calendar-options-icon-button []
  [:button#calendar-options.mdl-button.mdl-js-button.mdl-button--icon
   [icon "view_day"]])

(defn- calendar-options-menu [state-ref]
  (into [:ul.mdl-menu.mdl-menu--bottom-right.mdl-js-menu.mdl-js-ripple-effect
         {:data-mdl-for "calendar-options"}]
        (map (fn [[k v]]
               [mdl-menu-item #(set-view! state-ref k) (:string v)])
             view-map)))

(defn calendar-options
  "Calendar options component"
  [state-ref]
  (r/create-class
    {:reagent-render
     (fn [state-ref]
       [:div
        (calendar-options-icon-button)        
        [calendar-options-menu state-ref]])
     :component-did-mount
     (fn [this]
       (upgrade-dom! state-ref))}))

(defn edit-dialog-empty-event [time]
  {:event/type 1
   :event/start time
   :event/end (+ time (* 1000 60 60))})

;; rename, hehe
(defn add-button-icon-button []
  [:button#add-button.mdl-button.mdl-js-button.mdl-button--icon
   [icon "add"]])

(defn add-button [state-ref]
  [link (fn []
          (swap! state-ref assoc-in [:dialogs :edit-dialog :event] (edit-dialog-empty-event
                                                                     (get-current-time state-ref)))
          (prn (-> @state-ref :dialogs))
          (set-dialog! state-ref :edit-dialog))
        [add-button-icon-button]])

(defn- event-border-radius-str [event]
  (let [top-radius     (if (-> event :nox/clamp :start)
                         0
                         event-radius)
        bottom-radius  (if (-> event :nox/clamp :end)
                         0
                         event-radius)]
    (str top-radius "px "
         top-radius "px "
         bottom-radius "px "
         bottom-radius "px")))

(defn- event-clamp-style [event]
  (let [radius-str (event-border-radius-str event)]
    {:-webkit-border-radius radius-str
     :-moz-border-radius    radius-str
     :border-radius         radius-str}))

(defn- event-key [event]
  (str "event-" (:grid/row event) "-" (:grid/column event) "-" (entity-kw event)))

(defn- event-tooltip-key [event]
  (str "event-tooltip-" (:grid/row event) "-" (:grid/column event) "-" (entity-kw event)))

(def popup-delta (atom nil))

(defn event->tooltip [state-ref event]
  (let [popup-layer (r/cursor state-ref [:popup-layer])]
    (m/on "calendar-move" (fn [[_ {:keys [x y]}]]
                            (when (bubble/added? popup-layer ::tooltip)
                              (bubble/move popup-layer
                                           ::tooltip
                                           (+ x (:x @popup-delta))
                                           (+ y (:y @popup-delta))))))
    (m/on "calendar-resize" #(bubble/hide popup-layer ::tooltip))
    (fn [state-ref event]
      [:div.nox-tooltip.triangle-isosceles.top {:style {:width 300}}
       [:div.mdl-button.mdl-js-button.mdl-button--icon {:style {:float :right}}
        [:i {:class :material-icons
             :style {:font-size 24
                     :cursor :pointer}
             :on-click #(bubble/hide (r/cursor state-ref [:popup-layer]) ::tooltip)}
         "close"]]
       [:table
        [:tbody
         [:tr.widget-tr
          [:td.information-label "Title"]
          [:td.popup-td (or (not-empty (:event/title event)) "(no title)")]]
         [:tr.widget-tr
          [:td.information-label "Description"]
          [:td.popup-td (or (not-empty (:event/desc event)) "(no description)")]]
         [:tr.widget-tr
          [:td.information-label "Start time"]
          [:td.popup-td (.format (js/moment (:event/start event)) "LLLL")]]
         [:tr.widget-tr
          [:td.information-label "End time"]
          [:td.popup-td (.format (js/moment (:event/end event)) "LLLL")]]
     ;    [:tr.widget-tr
     ;     [:td.information-label "Where"]
     ;     [:td.popup-td "Hangout"]]
     ;    [:tr.widget-tr
     ;     [:td.information-label "Created for"]
     ;     [:td.popup-td "lmartin761@gmail.com"]]
     ;    [:tr.widget-tr
     ;     [:td.information-label "Guest"]
     ;     [:td.popup-td "Sonny"]]]]
     ;  [:div.assistance-footer
     ;   [:span
     ;    [:span "Going?"]
     ;    [:a.label-option {:href ""} "Yes"]
     ;    [:a.label-option {:href ""} "Maybe"]
     ;    [:a.label-option {:href ""} "No"]]]
       ]]


       [:button.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--accent.popup-button
        {:on-click (fn [_]
                     (swap! state-ref assoc-in [:dialogs :edit-dialog :event]
                       (event-grid/normalize-event
                         (db/query-event-by-entity-kw state-ref entity-kw (entity-kw event)))) ;; think whether associng current event is enough
                     (set-dialog! state-ref :edit-dialog))}
        "More"]
       [:button.mdl-button.mdl-js-button.mdl-button--raised.popup-button
        {:on-click (fn [_]
                     (swap! state-ref assoc-in [:dialogs :agenda-view-delete-dialog entity-kw] (entity-kw event))
                     (set-dialog! state-ref :agenda-view-delete-dialog))} 
        "Delete"]])))

;; TODO move this somewhere

(defn render-event [state-ref event left-margin top-margin]
      (let [drawable-area-width  (drawable-area-width state-ref)
            drawable-area-height (drawable-area-height state-ref)
            width                (- (:width event) (* 2 horizontal-event-padding))
            height               (max 18 (- (:height event) (* 2 vertical-event-padding)))
            hover?               (get-in @state-ref [:event-params (:system/id event) :hover?])
            popup-layer          (r/cursor state-ref [:popup-layer])]
        [:div {:key   (event-key event)
               :id    (event-key event)
               :on-mouse-enter  (fn [_]
                                  (swap! state-ref assoc-in [:event-params (:system/id event) :hover?] true))
               :on-mouse-leave  (fn [_]
                                  (swap! state-ref assoc-in [:event-params (:system/id event) :hover?] false))
               :style (merge (event-clamp-style event)
                             {:position        :absolute
                              :border          "1px solid white"
                              :height          height
                              :width           width 
                              :max-height      height 
                              :max-width       width 
                              :left            (+ left-margin (:x event))
                              :top             (+ top-margin (:y event))
                              :padding-left    horizontal-event-padding
                              :padding-right   horizontal-event-padding
                              :padding-top     vertical-event-padding
                              :padding-bottom  vertical-event-padding
                              :z-index         (if hover?
                                                 (-> @state-ref :event-count)
                                                 (:nox/z-index event))}
                             (when hover?
                               {:background-color :navy}))
               :class (str "event" (if (= 0 (:event/type event))
                                     " event-disabled"
                                     " event-enabled"))
               :on-click  (fn [js-ev]
                            (let [x (- (.-clientX js-ev) 64)
                                  y (- (.-clientY js-ev) 56)]
                              (reset! popup-delta {:x (- x (-> @state-ref :app/xy :x))
                                                   :y (- y (-> @state-ref :app/xy :y))})
                              (bubble/add popup-layer
                                          {:id ::tooltip
                                           :class :calendar-tooltip
                                           :view [event->tooltip state-ref event]})
                              (bubble/show popup-layer ::tooltip x y)))}
         [:div {:style {:position    :relative
                        :left        2
                        :top         2
                        :height      (max 14 (- height double-event-margin 6))
                        :width       (- width double-event-margin 4)
                        :max-height  (max 14 (- height double-event-margin 6))
                        :max-width   (- width double-event-margin 4)
                        :font-size   11
                        :line-height "13px"
                        :overflow    :hidden
                        ;:word-wrap   :break-word
                        :text-overflow :ellipsis
                        }}
          (into [:a (merge {:style {}
                            :href "#"
                            :data-tooltip false}
                           (when-not (= 0 (:event/type event))
                             {:on-click (fn [_]
                                          (when (entity-kw event)
                                            (let [event (event-grid/normalize-event
                                                          (db/query-event-by-entity-kw state-ref entity-kw (entity-kw event)))]
                                              (swap! state-ref assoc-in [:dialogs :edit-dialog :event] event)
                                              (set-dialog! state-ref :edit-dialog))))}))]
           (let [title (not-empty (:event/title event))
                 desc (not-empty (:event/desc event))]
             (if (or title desc)
               [(when title
                  [:strong title [:br]])
                desc]
                 ["(no title or description)"])))]]))

(defn event-wrapper []
  (let [this (r/current-component)]
    (into [:div (r/merge-props (r/props this)
                               {:style {:padding 20}})]
          (r/children this))))

(defn render-events [state-ref events left-margin top-margin]
  (let [c (count events)]
    (swap! state-ref assoc :event-count c)
    (map (fn [event]
           [render-event state-ref event left-margin top-margin])
         events)))

(defn double-click-listener [state-ref]
  (fn [js-ev]
    (js/console.log "double-click on" (.-target js-ev))
    (when (= (.-target js-ev) (-> @state-ref :refs :renderer))
      (let [rel-pos (gstyle/getRelativePosition js-ev (.-target js-ev))
            x (.-x rel-pos)
            y (.-y rel-pos)
            width (.-offsetWidth (.-target js-ev))
            height (.-offsetHeight (.-target js-ev))]
        (when (< first-column-width x))
          (let [[start-time end-time] ((-> @state-ref :view :position->time-range)
                                       state-ref x y width height)]
            (swap! state-ref assoc-in [:dialogs :edit-dialog :event]
              {:event/type  1
               :event/start start-time
               :event/end   end-time})
            (set-dialog! state-ref :edit-dialog))))))


(defn double-tap-listener [state-ref]
  (fn [js-ev]
    (when (and (= (.-target js-ev) (-> @state-ref :refs :renderer))
      (let [rect (.. js-ev -target getBoundingClientRect)
            x    (- (aget js-ev "center" "x") (.-left rect))
            y    (- (aget js-ev "center" "y") (.-top rect))
            w    (- (.-right rect) (.-left rect))
            h    (- (.-bottom rect) (.-top rect))]
        (when (< first-column-width x))
          (let [[start-time end-time] ((-> @state-ref :view :position->time-range) state-ref x y w h)]
            (swap! state-ref assoc-in [:dialogs :edit-dialog :event]
              {:event/type  1
               :event/start start-time
               :event/end   end-time})
            (set-dialog! state-ref :edit-dialog)))))))

(defn renderer-parent [state-ref]
  (r/create-class
    {:component-did-mount
     (fn [this]
       (swap! state-ref assoc-in [:refs :renderer-parent] (r/dom-node this)))
     :component-will-unmount
     (fn [this]
       (swap! state-ref update :refs dissoc :renderer-parent))
     :reagent-render
     (fn [state-ref]
       (let [main-component-parent (-> @state-ref :refs :main-component-parent)
             title-bar (-> @state-ref :refs :title-bar)]
         ;; hack to prevent flickering when other components are not yet mounted
         (if (and main-component-parent title-bar)
           (let [title-bar-height             (-> title-bar (.getClientRects) (aget 0) .-height)
                 main-component-parent-height (-> main-component-parent .-clientHeight)]
             (into [:div {:style {:position   :absolute
                                  :width      "100%"
                                  :height     (- main-component-parent-height title-bar-height)
                                  :overflow-x :auto
                                  :overflow-y :auto}}]
               (rest (r/children (r/current-component)))))
           [:div {:style {:position :absolute
                          :width "100%"
                          :height "100%"}}])))}))
                  ;
(defn renderer [state-ref]
  (r/create-class
    {:reagent-render
     (fn [state-ref]
       (into [:div.renderer {:style {:width  (drawable-area-width state-ref)
                                     :height (drawable-area-height state-ref)}
                             :on-double-click (double-click-listener state-ref)}]
         (rest (r/children (r/current-component)))))
     :component-did-mount
     (fn [this]
       (let [element (r/dom-node this)]
         (when (:mobile? @state-ref)
           (doto (js/Hammer. element #js {"preventDefault" true})
             (.. (remove "tap"))
             (.. (add (js/Hammer.Tap. #js {"event" "doubletap"
                                           "taps" 2
                                           "threshold" 20
                                           "posThreshold" 20})))
             (.. (on "doubletap" (double-tap-listener state-ref)))))
         (swap! state-ref assoc-in [:refs :renderer] element))
     )
     :component-will-unmount
     (fn [this]
       (js/console.log "unmounting")
       (swap! state-ref update :refs dissoc :renderer))
     }))

(defn events->hiccup [state-ref events left-margin top-margin]
  (into [:div] (render-events state-ref events left-margin top-margin)))

;;; TODO finish this
;(defn event-grid-promise [state-ref event-grid-kw]
;  (let [out (promise-chan)]
;    (go
;      (>! out (case event-grid-kw
;                :daily-event-grid (event-grid state-ref :day))))))  


(defn title-bar [state-ref]
  (r/create-class
   {:component-did-mount
    (fn [this]
      (swap! state-ref assoc-in [:refs :title-bar] (r/dom-node this)))
    :reagent-render
    (fn [state-ref]
      (let [this (r/current-component)
            [_ & children] (r/children this)]
        (into [:div (r/merge-props
                      {:class "title-bar mdl-card__title mdl-color--primary mdl-color-text--white mdl-card--border"}
                      (r/props this))]
              children)))}))


;; defaults for views
;; ----------------------------------------------------------------------------
(defn default-view [state-ref contents boundary]
  [:div {:on-mouse-down #(bubble/hide (r/cursor state-ref [:popup-layer]) ::tooltip)
         :on-scroll #(bubble/hide (r/cursor state-ref [:popup-layer]) ::tooltip)}
   [title-bar state-ref
    (switcher state-ref boundary)
    [:div.mdl-layout-spacer]
    [add-button state-ref]
    [calendar-options state-ref]]
   [renderer-parent state-ref
    [renderer state-ref contents]]])

(defn default-event-grid-args [state-ref unit]
  (merge (:view @state-ref)
         {:unit unit
          :time (get-current-time state-ref)
          :limited-width? (:limited-width? @state-ref)
          :drawable-area-width (drawable-area-width state-ref)
          :drawable-area-height (drawable-area-height state-ref)}))

(defn- default-row-caption-props [state-ref time y]
  (let [row-height (/ (drawable-area-height state-ref) 24)]
    {:key   (str "hour-" time)
     :style {:position :absolute
             :font-size "0.75em"
             :vertical-align :middle
             :text-align :right
             :top (* y row-height)
             :height row-height
             :width (* 0.9 first-column-width)
             :line-height (str row-height "px") ;; number doesn't work as expected
             }}))

(defn default-row-caption [state-ref time y]
  [:div (default-row-caption-props state-ref time y)
    (.. (js/moment time)
        (format hour-format-string))])


;; horizontal lines
;; ----------------------------------------------------------------------------
(defn default-horizontal-line-props [state-ref y n]
  {:key   (str "line-h-" y)
   :style {:position :absolute
           :background-color :darkgray
           :left 0
           :top (-> (/ y n) (* (dec (drawable-area-height state-ref))))
           :height 1
           :width (drawable-area-width state-ref)}})

(defn default-horizontal-line [state-ref y n]
  [:div (default-horizontal-line-props state-ref y n)])


;; vertical lines
;; ----------------------------------------------------------------------------
(defn default-vertical-line-props [state-ref x n left-margin]
  {:key   (str "line-v-" x)
   :style {:position :absolute
           :background-color :darkgray
           :left (-> (/ x n)
                     (* (- (dec (drawable-area-width state-ref)) left-margin))
                     (+ left-margin))
           :top 0
           :height (drawable-area-height state-ref)
           :width 1}})

(defn default-vertical-line [state-ref x n left-margin]
  [:div (default-vertical-line-props state-ref x n left-margin)])


;; daily view
;; ---------------------------------------------------------------------------- 
(defn daily-view-empty-grid [time]
  (let [day-start-time (start-of time :day)
        day-end-time (end-of time :day)]
    {[day-start-time day-end-time]
     {[day-start-time day-end-time] []}}))

(defn daily-view-table [state-ref empty-grid]
  (let [[start-time end-time] (first (keys empty-grid))
        rows (butlast (time-range start-time end-time :hour))
        columns (get-current-time state-ref)]
   (reduce into [:div [default-vertical-line state-ref 0 1 0]
                      [default-vertical-line state-ref 1 1 0]
                      [default-horizontal-line state-ref 24 24]]
                (into (map-indexed (fn [i time]
                                     [(default-horizontal-line state-ref i 24)
                                      (default-row-caption state-ref time i)])
                                   rows)
                      [[[default-vertical-line state-ref 0 7 first-column-width]]]))))
                               

(defn daily-view [state-ref]
  (let [current-time (get-current-time state-ref)
        empty-grid   (daily-view-empty-grid current-time)
        events-grid  (event-grid/grid state-ref empty-grid (default-event-grid-args state-ref :day))]
    [default-view state-ref
                  [:div 
                   [daily-view-table state-ref empty-grid]
                   [events->hiccup state-ref events-grid first-column-width 0]]
                  :day]))


;; weekly view
;; ----------------------------------------------------------------------------
(defn weekly-view-empty-grid [time]
  (let [week-start-time (start-of time :week)
        week-end-time (end-of time :week)]
    (reduce (fn [acc e]
              (assoc acc e {e []}))   
            {}
            (timespans week-start-time week-end-time :day))))

(defn weekly-view-table [state-ref empty-grid]
  (let [[start-time end-time] (first (keys empty-grid))
        rows (butlast (time-range start-time end-time :hour))
        columns (map (comp ffirst keys) (vals empty-grid))]
    (reduce into [:div [default-vertical-line state-ref 0 1 0]
                       [default-vertical-line state-ref 1 1 0]
                       [default-horizontal-line state-ref 24 24]]
                 (into (map-indexed (fn [x time]
                                      [[default-vertical-line state-ref x 7 first-column-width]])
                                    columns)
                       (map-indexed (fn [y time]
                                      [[default-horizontal-line state-ref y 24]
                                       [default-row-caption state-ref time y]])
                                    rows)))))

(defn weekly-view [state-ref]
  (let [current-time (get-current-time state-ref)
        empty-grid   (weekly-view-empty-grid current-time)
        events-grid  (event-grid/grid state-ref empty-grid (default-event-grid-args state-ref :week))]
    [default-view state-ref
                  [:div [weekly-view-table state-ref empty-grid]
                        [events->hiccup state-ref events-grid first-column-width 0]]
                  :week]))


;; monthly view
;; ----------------------------------------------------------------------------
(defn monthly-view-empty-grid [time]
  (let [month-start-time (-> time (start-of :month) (start-of :week))
        month-end-time (-> time (end-of :month) (end-of :week))]
    (reduce (fn [acc [week-start-time week-end-time :as e]]
              (assoc acc e (zipmap (timespans week-start-time week-end-time :day)
                                   (repeat []))))
            {}
            (timespans month-start-time month-end-time :week))))

(defn monthly-view-cell [state-ref time month-start-time month-end-time y x]
  (let [cell-width   (/ (drawable-area-width state-ref) 7)
        cell-height  (/ (drawable-area-height state-ref) 6)
        common-style {:left   (* x cell-width)
                      :top    (* y cell-height)
                      :width  (- cell-width 10)
                      :height (- cell-height 6)
                      :padding "3px 5px"
                      :z-index -100
                      :position :absolute
                      :font-size "0.75em"}
        in-other-month? (or (< time month-start-time)
                            (> time month-end-time))]
   [:div {:key   (str "day-" time)
          :class (when in-other-month?
                   :other-month)
          :style common-style}
    (-> time
        (js/moment)
        (.format "D")
        (js/parseInt))]))

(defn monthly-view-table [state-ref empty-grid]
  (let [time       (get-current-time state-ref)
        start-time (-> time (start-of :month) (start-of :week))
        end-time   (+ start-time (* (unit-duration :day) 7 6))
        cells      (butlast (time-range start-time end-time :day))
        month-start-time (start-of time :month)
        month-end-time   (end-of time :month)]
    (reduce into [:div]
      (into
        (into
          (map-indexed (fn [i time]
                         [[monthly-view-cell state-ref time month-start-time month-end-time (int (/ i 7)) (rem i 7)]])
                       cells)
          (map (fn [i]
                 [[default-horizontal-line state-ref i 6]])
               (range 7)))
       (map (fn [i]
              [[default-vertical-line state-ref i 7 0]])
            (range 8))))))


(defn monthly-view [state-ref]
  (let [current-time (get-current-time state-ref)
        empty-grid   (monthly-view-empty-grid current-time)
        events-grid  (event-grid/grid state-ref empty-grid (default-event-grid-args state-ref :month))]
    [default-view state-ref
                  [:div [monthly-view-table state-ref empty-grid]
                        [events->hiccup state-ref events-grid 0 0]]
                  :month]))



;; add dialog
;; ----------------------------------------------------------------------------
(defn get-date* [time]
  (let [m (js/moment time)]
    (+ (.valueOf m)
       (* 1000 60 (.utcOffset m)))))

(defn get-time* [time]
  (let [m (js/moment time)]
    (* 1000 (+ (* 60 (.minutes m))
               (* 60 60 (.hours m))))))

(defn edit-dialog-save-button [state-ref event]
  [:button.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--accent
   {:on-click (fn [_]
                (edit-or-add-event state-ref event)
                (set-dialog! state-ref nil))}
   "Save"])

(defn edit-dialog-cancel-button [state-ref]
  [:button.mdl-button.mdl-js-button.mdl-button--raised
   {:on-click (fn [_]
                (set-dialog! state-ref nil))}
   "Cancel"])

(def non-blank-regexp ".*\\S+.*")

(defn edit-dialog-event-title [event]
  (r/create-class
    {:reagent-render
     (fn [event]
       [:div.mdl-cell.mdl-cell--11-col.mdl-textfield.mdl-js-textfield
        {:on-change (fn [js-ev]
                      (swap! event assoc :event/title (.. js-ev -target -value)))}
        [:input#edit-dialog-event-title.mdl-textfield__input {:type "text" :required true :pattern non-blank-regexp}]
        [:label.mdl-textfield__label {:for "title"} "Event title"]
        [:span.mdl-textfield__error "Title can't be blank"]])
     :component-did-mount
     (fn [this]
       (aset (.getElementById js/document "edit-dialog-event-title") "value" (:event/title @event)))}))

;; TODO find a better name
;(defn- call-when-not-NaN [n f]
;  (if (js/isNaN n)
;    NaN
;    (f n)))

(defn- edit-dialog-event-start-time-value []
  (let [a  (js/moment
                (+ (.. (.getElementById js/document "edit-dialog-event-start-date") -valueAsNumber)
                   (.. (.getElementById js/document "edit-dialog-event-start-time") -valueAsNumber)
                   (- (* 1000 60 (.utcOffset (js/moment))))))]
    (.valueOf a)))

(defn- edit-dialog-event-end-time-value []
  (let [a (js/moment
                (+ (.. (.getElementById js/document "edit-dialog-event-end-date") -valueAsNumber)
                   (.. (.getElementById js/document "edit-dialog-event-end-time") -valueAsNumber)
                   (- (* 1000 60 (.utcOffset (js/moment))))))]
    (.valueOf a)))

(defn edit-dialog-event-start-date [event] 
  (r/create-class
    {:reagent-render
     (fn [event]
       [:div.mdl-cell.mdl-cell--3-col.mdl-textfield.mdl-js-textfield 
        {:on-change (fn [js-ev]
                      (swap! event assoc :event/start (edit-dialog-event-start-time-value)))}
        [:input#edit-dialog-event-start-date.mdl-textfield__input {:type "date" :required true}]
        [:span.mdl-textfield__error "Incorrect date"]])
     :component-did-mount
     (fn [this]
       (aset (.getElementById js/document "edit-dialog-event-start-date") "valueAsNumber" (get-date* (:event/start @event))))}))

(defn edit-dialog-event-start-time [event]
  (r/create-class
    {:reagent-render
     (fn [event]
       [:div.mdl-cell.mdl-cell--2-col.mdl-textfield.mdl-js-textfield
        {:on-change (fn [js-ev]
                      (swap! event assoc :event/start (edit-dialog-event-start-time-value)))}
        [:input#edit-dialog-event-start-time.mdl-textfield__input {:type "time" :required true}]
        [:span.mdl-textfield__error "Incorrect time"]])
     :component-did-mount
     (fn [this]
       (aset (.getElementById js/document "edit-dialog-event-start-time") "valueAsNumber" (get-time* (:event/start @event))))}))


(defn edit-dialog-event-end-date [event]
  (r/create-class
    {:reagent-render
     (fn [event]
       [:div.mdl-cell.mdl-cell--3-col.mdl-textfield.mdl-js-textfield 
        {:on-change (fn [js-ev]
                      (swap! event assoc :event/end (edit-dialog-event-end-time-value)))}
        [:input#edit-dialog-event-end-date.mdl-textfield__input {:type "date" :required true}]
        [:span.mdl-textfield__error "Incorrect date"]])
     :component-did-mount
     (fn [this]
       (aset (.getElementById js/document "edit-dialog-event-end-date") "valueAsNumber" (get-date* (:event/end @event))))}))


(defn edit-dialog-event-end-time [event]
  (r/create-class
    {:reagent-render
     (fn [event]
       [:div.mdl-cell.mdl-cell--2-col.mdl-textfield.mdl-js-textfield
        {:on-change (fn [js-ev]
                      (swap! event assoc :event/end (edit-dialog-event-end-time-value)))}
        [:input#edit-dialog-event-end-time.mdl-textfield__input {:type "time" :required true}]
        [:span.mdl-textfield__error "Incorrect time"]])
     :component-did-mount
     (fn [this]
       (aset (.getElementById js/document "edit-dialog-event-end-time") "valueAsNumber" (get-time* (:event/end @event))))}))

(defn edit-dialog-event-desc [event]
  (r/create-class
    {:reagent-render
     (fn [event]
       [:div.mdl-cell.mdl-cell--10-col.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
        {:on-change (fn [js-ev]
                      (swap! event assoc :event/desc (.. js-ev -target -value)))}
        [:input#edit-dialog-event-desc.mdl-textfield__input {:type "text"}]
        [:label.mdl-textfield__label {:for "Description"} "Description"]])
     :component-did-mount
     (fn [this]
       (aset (.getElementById js/document "edit-dialog-event-desc") "value" (:event/desc @event)))}))

(defn edit-dialog [state-ref]
  (let [event (r/cursor state-ref [:dialogs :edit-dialog :event])]
    [:div.mdl-dialog__content {:style {:margin 0 :padding 0}}
     [:form#form1.mdl-grid
      [edit-dialog-event-title event]
;          [:div.mdl-cell.mdl-cell--1-col
;           [:button#color.mdl-button.mdl-js-button.mdl-button--icon
;            [:i#purple.material-icons.add-icons "palette"]]]
;          [:div.mdl-tooltip
;           {:for "color"}
;           "Event color"]
      [edit-dialog-event-start-date event]
      [edit-dialog-event-start-time event]
      [:div.mdl-cell.mdl-cell--2-col.date-text
       "to"]
      [edit-dialog-event-end-time event]
      [edit-dialog-event-end-date event]
;          [:div.mdl-cell.mdl-cell--12-col.event-checkbox
;           [:div.mdl-cell.mdl-cell--1-col
;            [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
;             {:for "list-checkbox-1"}
;             [:input#list-checkbox-1.mdl-checkbox__input
;              {:type "checkbox"}]]]
;           [:div.mdl-cell.mdl-cell--2-col.checkbox-text
;            "All day"]
;            [:div.mdl-cell.mdl-cell--1-col
;             [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
;              {:for "list-checkbox-2"}
;              [:input#list-checkbox-2.mdl-checkbox__input
;               {:type "checkbox"}]]]
;            [:div.mdl-cell.mdl-cell--2-col.checkbox-text
;             "Repeat"]
;            [:div.mdl-cell.mdl-cell--1-col
;             [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
;              {:for "list-checkbox-6"}
;              [:input#list-checkbox-6.mdl-checkbox__input
;               {:type "checkbox"}]]]
;            [:div.mdl-cell.mdl-cell--2-col.checkbox-text
;             "Notification"]]
;          [:div.mdl-cell.mdl-cell--12-col.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
;           [:input#Location.mdl-textfield__input {:type "text"}]
;           [:label.mdl-textfield__label {:for "Location"} "Location"]]
      [edit-dialog-event-desc event]]
;          [:div.mdl-cell.mdl-cell--1-col
;           [:button#videocall.mdl-button.mdl-js-button.mdl-button--icon
;            [:i.material-icons.add-icons "video_call"]]]
;          [:div.mdl-tooltip
;           {:for "videocall"}
;           "Add Video call"]
;          [:div.mdl-cell.mdl-cell--1-col
;           [:button#file.mdl-button.mdl-js-button.mdl-button--icon
;            [:i.material-icons.add-icons "attach_file"]]]
;          [:div.mdl-tooltip
;           {:for "file"}
;           "Attach file"]]
    ;  [:div.mdl-cell.mdl-cell--3-col.date-text
    ;   "Add guests"]
    ;  [:div.mdl-cell.mdl-cell--7-col.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
    ;   [:input#guests.mdl-textfield__input
    ;    {:pattern "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$",
    ;     :type "text"}]
    ;   [:label.mdl-textfield__label
    ;    {:for "guests"}
    ;    "Enter email addresses"]
    ;   [:span.mdl-textfield__error "Input is not a email!"]]
    ;  [:div.mdl-cell.mdl-cell--1-col
    ;   [:button#add-guest.mdl-button.mdl-js-button.mdl-button--icon
    ;    [:i.material-icons.add-icons "add_circle"]]]
    ;  [:div.mdl-cell.mdl-cell--3-col.date-text
    ;   "Guests can"]
    ;  [:div.mdl-cell.mdl-cell--1-col
    ;   [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
    ;    {:for "list-checkbox-3"}
    ;    [:input#list-checkbox-3.mdl-checkbox__input
    ;     {:type "checkbox"}]]]
    ;  [:div.mdl-cell.mdl-cell--2-col.checkbox-text
    ;   "Modify event"]
    ;  [:div.mdl-cell.mdl-cell--1-col
    ;   [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
    ;    {:for "list-checkbox-4"}
    ;    [:input#list-checkbox-4.mdl-checkbox__input
    ;     {:type "checkbox"}]]]
    ;  [:div.mdl-cell.mdl-cell--2-col.checkbox-text
    ;   "Invite others"]
    ;  [:div.mdl-cell.mdl-cell--1-col
    ;   [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
    ;    {:for "list-checkbox-5"}
    ;    [:input#list-checkbox-5.mdl-checkbox__input
    ;     {:type "checkbox"}]]]
    ;  [:div.mdl-cell.mdl-cell--2-col.checkbox-text
    ;   "See guest list"]]
     [edit-dialog-save-button state-ref @event]
     [edit-dialog-cancel-button state-ref]]))

(defn create-confirmation-dialog [text result-fn]
  (fn [state-ref]
    [:div.mdl-dialog__content
     [:p text]
     [:div.mdl-dialog__actions
      [:button.mdl-button {:type :button :on-click (fn [_]
                                                     (result-fn state-ref false)
                                                     (set-dialog! state-ref nil))}
       "No"]
      [:button.mdl-button {:type :button :on-click (fn [_]
                                                     (result-fn state-ref true)
                                                     (set-dialog! state-ref nil))}
       "Yes"]]]))

(def agenda-view-delete-dialog
  (create-confirmation-dialog
    "Are you sure you want to delete this event?"
    (fn [state-ref result]
      (when result
        (let [eid (get-in @state-ref [:dialogs :agenda-view-delete-dialog entity-kw])]
          (remove-event state-ref eid))))))

(defn agenda-event [state-ref event]
  [:tr.widget-tr.agenda-event
   [:td.date-agenda [:a.widget-a {:href ""} (:event/start event)]]
   [:td.time-agenda [:span (:event/end event)]]
   [:td.information
    [:div
     [:span.information-title
      (if (not-empty (:event/title event))
       [:strong (:event/title event)]
       (:event/desc event))]
     #_[:i.material-icons "notifications_active"]]]
 ;    [:tr [:td.information-label "Where"] [:td "Hangout"]]
     ;    [:tr
 ;     [:td.information-label "Created for"]
 ;     [:td "Somebody - fix this"]]
 ;    [:tr [:td.information-label "Guest"] [:td "Sonny"]]]
 ;   [:div.assistance-footer
 ;    [:span
 ;     [:span "Going?"]
 ;     [:a.label-option {:href ""} "Yes"]
 ;     [:a.label-option {:href ""} "Maybe"]
 ;     [:a.label-option {:href ""} "No"]]]]
   [:td.agenda-options
    [:button.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--accent
     {:on-click (fn [_]
                  (swap! state-ref assoc-in [:dialogs :edit-dialog :event]
                         (event-grid/normalize-event
                           (db/query-event-by-entity-kw state-ref entity-kw (entity-kw event)))) ;; think whether associng current event is enough
                  (set-dialog! state-ref :edit-dialog))}
     "More"]
    [:button.mdl-button.mdl-js-button.mdl-button--raised
     {:on-click (fn [_]
                    (swap! state-ref assoc-in [:dialogs :agenda-view-delete-dialog entity-kw] (entity-kw event))
                    (set-dialog! state-ref :agenda-view-delete-dialog))} 
     "Delete"]]])

(defn agenda-view-format-instant [inst]
  (-> inst event-grid/instant->number
      (js/moment)
      (.format "ddd DD MMM HH:mma")))

(defn agenda-view [state-ref]
  ;; temporary hack to force reagent update
  (js/console.log (:__force-update? @state-ref))
  (let [events (->> (db/query-events-by-timespan state-ref (js/moment))
                    (sort-by (juxt :event/start :event/end)))]
    [:div
     [title-bar state-ref
      [:h1.mdl-card__title-text "Agenda"]
      [:div.mdl-layout-spacer]
      [add-button state-ref]
      [calendar-options state-ref]]
     [renderer-parent state-ref
      [:div.agenda
       (if (seq events)
         [:table.widget-table
          (into [:tbody]
            (map (fn [event]
                   [agenda-event state-ref
                                 (-> event
                                     (update :event/start agenda-view-format-instant)
                                     (update :event/end agenda-view-format-instant))])
                 events))]
         agenda-view-empty?-content)]]]))

(declare update-area-size!)

(defn- view [state-ref]
  (r/create-class
    {:reagent-render
     (fn [state-ref]
       [(get-in @state-ref [:view :render-fn]) state-ref])
     :component-did-update
     (fn [_]
       (update-area-size! state-ref))}))

(defn dialog [state-ref]
  (let [initialized? (r/cursor state-ref [:dialog :initialized?])]
    (r/create-class
      {:reagent-render
        (fn [state-ref]
         (when-let [current-dialog (:dialog @state-ref)]
           [:dialog.mdl-dialog {:id "dialog"
                                :class (:name current-dialog)
                                }
            [(:render-fn current-dialog) state-ref]]))
        :component-did-update
        (fn [this _]
          (when (and (not @initialized?) (r/dom-node this))
            (when-let [element (r/dom-node this)]
              (aset element "oncancel" (fn [js-ev]
                                         (swap! state-ref dissoc :dialog)))
              (swap! state-ref assoc-in [:refs :dialog] element)
              (reset! initialized? true)))
          (when (and @initialized? (:dialog @state-ref) (not (-> @state-ref :dialog :displayed?)))
            (.showModal (-> @state-ref :refs :dialog))
            (upgrade-dom! state-ref)
            (swap! state-ref assoc-in [:dialog :displayed?] true)))})))


(defn default-ui [state-ref {:keys [view dialog]}]
  [:div.nox.mdl-card {:class (-> @state-ref :view :name)}
   [view state-ref]
   [dialog state-ref]])

(defn set-ui! [state-ref f]
  (swap! state-ref assoc :ui f))

(defn ui [state-ref m]
  [(:ui @state-ref default-ui) state-ref m])

(defn- update-area-size!
  "Adds current area size (clientWidth and clientHeight) to a specified state ref"
  [state-ref]
  (when-let [element (-> @state-ref :refs :renderer-parent)]
    (let [client-width (.-clientWidth element)
          client-height (.-clientHeight element)
          update-limited-width?-fn (get-in @state-ref [:view :update-limited-width?-fn])]
      (swap! state-ref assoc
        :drawable-area-width  (max 200 client-width)
        :drawable-area-height (max (* 24 (+ 18 2)) client-height
                                   #_(dec (- client-height (.-y (gstyle/getRelativePosition
                                                                 (-> @state-ref :refs :renderer-parent)
                                                                 (-> @state-ref :refs :main-component)))))))
      (when update-limited-width?-fn
        (update-limited-width?-fn state-ref)))))

(defn- add-default-listeners!
  "Adds default event listeners"
  [state-ref]
  (swap! state-ref assoc
         :resize-chan (m/on "calendar-resize" #(update-area-size! state-ref))
         :move-chan (m/on "calendar-move" (fn [[_ xy]]
                                            (swap! state-ref assoc :app/xy xy)))))

(defn- remove-default-listeners! [state-ref]
  (swap! state-ref update :resize-chan close!)
  (swap! state-ref update :move-chan close!))

(defn- create-initial-state!
  "Adds default values to a specified state"
  [state-ref] 
  (swap! state-ref assoc :limited-width? false ; this will be overriden by most of the views
                         :default-moment-format-str "dddd, DD MMMM YYYY"
                         :default-monthly-moment-format-str "MMM YYYY"))

(defn component
  "Creates main Nox component"
  [state-ref]
  (r/create-class
    {:component-will-mount
     (fn [this]
       (create-initial-state! state-ref)
       (create-mdl-load-channel! state-ref)
       (add-default-listeners! state-ref)
       (set-current-time! state-ref (.valueOf (js/moment)))
       (swap! state-ref assoc :mobile? (some? (.mobile (js/MobileDetect. js/window.navigator.userAgent))))
       (set-view! state-ref :daily-view)
       (m/on "calendar-move" (fn [[_ xy]]
                               (swap! state-ref assoc :app/xy xy)))
       (when-not (:remote-transact-fn @state-ref)
         (swap! state-ref assoc :remote-transact-fn (constantly nil))))
     :component-will-unmount
     (fn [this]
       (remove-default-listeners! state-ref)
       (swap! state-ref dissoc :refs))
     :component-did-mount
     (fn [this]
       (let [el (r/dom-node this)
             parent (gdom/getParentElement el)]
         ;(aset parent "style" "overflow" "auto")
         (swap! state-ref assoc-in [:refs :main-component] el)
         (swap! state-ref assoc-in [:refs :main-component-parent] parent)
         ;; find a better solution. It seems that not all elements are loaded before calling update-area-size
         (js/setTimeout
           (fn []
             (update-area-size! state-ref)
           2000))))
     :reagent-render
     (fn [state-ref]
       (js/console.log "state-ref" state-ref)
       [ui state-ref {:view view
                      :dialog dialog}])}))

(def ^{:doc "Compatibility def"}
  render component)

