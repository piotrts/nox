(ns nox-demo.core
  (:require [com.kaicode.nox.core :as nox]
            [reagent.core :as r]))

(defonce
  ^{:doc "app-state for demo purposes"}
  app-state (r/atom {}))

(defn render-nox-demo []
  (r/render
   [nox/component app-state]
   (.getElementById js/document "app")))

(render-nox-demo)

(defn on-js-reload []
  ;; ...
  )

